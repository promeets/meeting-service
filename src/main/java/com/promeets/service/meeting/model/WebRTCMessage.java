package com.promeets.service.meeting.model;

import lombok.Data;

@Data
public class WebRTCMessage {
    private String chatId;
    private String from;
    private String to;
    private String type;
    private String data;
}
