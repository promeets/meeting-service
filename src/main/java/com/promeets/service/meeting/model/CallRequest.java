package com.promeets.service.meeting.model;

import lombok.Data;

@Data
public class CallRequest {
    private String from;
    private String to;
    private String chatId;
}
