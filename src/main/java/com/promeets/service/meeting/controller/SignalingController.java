package com.promeets.service.meeting.controller;

import com.promeets.service.meeting.ActiveParticipantsService;
import com.promeets.service.meeting.model.CallRequest;
import com.promeets.service.meeting.model.WebRTCMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;


@Controller
public class SignalingController {

    private final SimpMessagingTemplate simpMessagingTemplate;
    private final ActiveParticipantsService activeParticipantsService;

    @Autowired
    public SignalingController(SimpMessagingTemplate simpMessagingTemplate, ActiveParticipantsService activeParticipantsService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.activeParticipantsService = activeParticipantsService;
    }

    @MessageMapping("/callrequest/{userId}")
    public void callRequest(@DestinationVariable String userId, CallRequest callRequest) {
        activeParticipantsService.addParticipantToActive(callRequest.getChatId(), callRequest.getFrom());
        simpMessagingTemplate.convertAndSend("/topic/callrequest/" + callRequest.getTo(), callRequest);
    }

//    @MessageMapping("/offer/{userId}")
//    public void offer(@DestinationVariable String userId, WebRTCMessage message) {
//        simpMessagingTemplate.convertAndSend("/topic/offer/" + message.getTo(), message);
//    }
//
//    @MessageMapping("/answer/{userId}")
//    public void answer(@DestinationVariable String userId, WebRTCMessage message) {
//        simpMessagingTemplate.convertAndSend("/topic/answer/" + message.getTo(), message);
//    }
//
//    @MessageMapping("/candidate/{userId}")
//    public void candidate(@DestinationVariable String userId, WebRTCMessage message) {
//        simpMessagingTemplate.convertAndSend("/topic/candidate/" + message.getTo(), message);
//    }
//
//    @MessageMapping("/leave/{userId}")
//    public void leave(@DestinationVariable String userId, WebRTCMessage message) {
//        simpMessagingTemplate.convertAndSend("/topic/leave/" + message.getTo(), message);
//    }

    @MessageMapping("/offer/{chatId}")
    public void offer(@DestinationVariable String chatId, WebRTCMessage message) {
        activeParticipantsService.addParticipantToActive(chatId, message.getFrom());
        simpMessagingTemplate.convertAndSend("/topic/offer/" + chatId, message);
    }

    @MessageMapping("/answer/{chatId}")
    public void answer(@DestinationVariable String chatId, WebRTCMessage message) {
        simpMessagingTemplate.convertAndSend("/topic/answer/" + chatId, message);
    }

    @MessageMapping("/candidate/{chatId}")
    public void candidate(@DestinationVariable String chatId, WebRTCMessage message) {
        simpMessagingTemplate.convertAndSend("/topic/candidate/" + chatId, message);
    }

    @MessageMapping("/leave/{chatId}")
    public void leave(@DestinationVariable String chatId, WebRTCMessage message) {
        activeParticipantsService.removeParticipantFromActive(chatId, message.getFrom());
        simpMessagingTemplate.convertAndSend("/topic/leave/" + chatId, message);
    }

}
