package com.promeets.service.meeting.controller;

import com.promeets.service.meeting.ActiveParticipantsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class ActiveParticipantsRestController {

    private final
    ActiveParticipantsService activeUsersService;

    @Autowired
    public ActiveParticipantsRestController(ActiveParticipantsService activeUsersService) {
        this.activeUsersService = activeUsersService;
    }

    @RequestMapping(method = RequestMethod.GET, path = "active/participants/{chatId}")
    public List<String> getActiveParticipantsByChatId(@PathVariable("chatId") String chatId) {
        return activeUsersService.getActiveParitcipantsByChatId(chatId);
    }
}
