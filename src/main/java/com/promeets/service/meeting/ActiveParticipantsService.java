package com.promeets.service.meeting;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ActiveParticipantsService {
    private Map<String, List<String>> calls;

    public ActiveParticipantsService() {
        calls = new HashMap<>();
    }

    public List<String> getActiveParitcipantsByChatId(String chatId) {
        return calls.get(chatId);
    }

    public void removeParticipantFromActive(String chatId, String participantId) {
        calls.get(chatId).remove(participantId);
    }

    public void addParticipantToActive(String chatId, String participantId) {
        if(!calls.containsKey(chatId))
            calls.put(chatId, new ArrayList<>());
        if(!calls.get(chatId).contains(participantId))
            calls.get(chatId).add(participantId);
    }
}
